#!/usr/bin/env python3

import rclpy
import math
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray #String 
from sensor_msgs.msg import JointState, Range  
########################################################################### importer le Type Range (laser topic type is Range)
# import only system from os
from os import system, name
  
# import sleep to show output for some time period
#from time import sleep
#
##########################################################################

class EPuckController(Node):
    def __init__(self):
        super().__init__('epuck_controller')

        self.cmd_publisher_ = self.create_publisher(
            Float64MultiArray, '/wheels_velocity_controller/commands', 10)





        self.joint_state_subscription_ = self.create_subscription(
            JointState,
            '/joint_states',
            self.joint_state_callback,
            10)

##########################################################################
#Fonction de souscription au Topic Laser 
##########################################################################

 # Laser 0
        self.Laser0_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps0/out',                                                  # Topic name 
            self.Laser0_Value,                                           # call function Laser1Value
            10)
##########################################################################

 # Laser 1
        self.Laser1_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps1/out',                                                  # Topic name 
            self.Laser1_Value,                                           # call function Laser1Value
            10)
##########################################################################


 # Laser 2
        self.Laser2_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps2/out',                                                  # Topic name 
            self.Laser2_Value,                                           # call function Laser1Value
            10)
##########################################################################

 # Laser 3
        self.Laser3_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps3/out',                                                  # Topic name 
            self.Laser3_Value,                                           # call function Laser1Value
            10)
##########################################################################

 # Laser 4
        self.Laser4_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps4/out',                                                  # Topic name 
            self.Laser4_Value,                                           # call function Laser1Value
            10)
##########################################################################

 # Laser 5
        self.Laser5_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps5/out',                                                  # Topic name 
            self.Laser5_Value,                                           # call function Laser1Value
            10)
##########################################################################

 # Laser 6
        self.Laser6_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps6/out',                                                  # Topic name 
            self.Laser6_Value,                                           # call function Laser1Value
            10)
##########################################################################

 # Laser 7
        self.Laser7_state_subscription_ = self.create_subscription(
            Range,                                                       # Type of message received
            '/ps7/out',                                                  # Topic name 
            self.Laser7_Value,                                           # call function Laser1Value
            10)
##########################################################################

 # Robot_Description 7
#        self.RobotDes_state_subscription_ = self.create_subscription(
#            String,                                                       # Type of message received
#            '/robot_description',                                                  # Topic name 
#            self.RobDes,                                                  # call function Laser1Value
#            10)
##########################################################################



        self.cmd_ = Float64MultiArray(data=[0, 0])

        self.start_time_ = self.get_clock().now().seconds_nanoseconds()[0]
        self.run_timer_ = self.create_timer(0.1, self.run)
        



#########################################################################
                    # Create function to call 
#########################################################################

    def Laser0_Value(self, msg):
        self.Laser0val= msg.range
        self.get_logger().info(
            "\n\tLaser 0 : %s" %
            (self.Laser0val))

    def Laser1_Value(self, msg):
        self.Laser1val= msg.range
        self.get_logger().info(
            "\n\tLaser 1 : %s" %
            (self.Laser1val))


    def Laser2_Value(self, msg):
        self.Laser2val= msg.range
        self.get_logger().info(
            "\n\tLaser 2 : %s" %
            (self.Laser2val))

    def Laser3_Value(self, msg):
        self.Laser3val= msg.range
        self.get_logger().info(
            "\n\tLaser 3 : %s" %
            (self.Laser3val))


    def Laser4_Value(self, msg):
        self.Laser4val= msg.range
        self.get_logger().info(
            "\n\tLaser 4 : %s" %
            (self.Laser4val))


    def Laser5_Value(self, msg):
        self.Laser5val= msg.range
        self.get_logger().info(
            "\n\tLaser 5 : %s" %
            (self.Laser5val))

    def Laser6_Value(self, msg):
        self.Laser6val= msg.range
        self.get_logger().info(
            "\n\tLaser 6 : %s" %
            (self.Laser6val))

    def Laser7_Value(self, msg):
        self.Laser7val= msg.range
        self.get_logger().info(
            "\n\tLaser 7 : %s" %
            (self.Laser7val))



    def joint_state_callback(self, msg):
        self.PosR=msg.position[0]
        self.PosL=msg.position[1]
        self.vitesse=(msg.velocity[0]+msg.velocity[1])/2
        self.get_logger().info(
            "\nWheels\n\tname: %s\n\tposition: %s\n\tvelocity: %s" %
            (msg.name, msg.position, msg.velocity))




##########################################################################
              # Function with Boolean to set movement & Direction
##########################################################################


    
##########################################################################
#0 from back left to 7 in back right (front is the 2 seperate sensors (positive mvt))
##########################################################################

    def run(self):

        #self.Laser0val # on recupere la valeur du laser 1 et on l'utilise.'
#        posinitx=0
#        posinity=0
#        posDesx=0
#        posDesy=0

        if self.get_clock().now().seconds_nanoseconds()[0] > self.start_time_ + 0.1:


#Angle      
            
            
            AngleDes=45
            DistanceDes=100
            Pangle=0.0
            Anglecom=AngleDes/20.5          # Precision Angle
            Pangle=self.PosR-self.PosL      # ~16 = 360 = 2*pi

            
# Vitesse et Distance
            D_time=self.get_clock().now().seconds_nanoseconds()[0]-self.start_time_
                          
            Distance=self.vitesse*D_time
            Speed=(DistanceDes-Distance)/D_time
            
            if Distance < DistanceDes-0.1:  #1 Precision
                if Pangle < Anglecom - 0.05:
                    self.cmd_.data = [0.5,-0.5] # 
               
                else: 
                    if Pangle > Anglecom + 0.05:
                        self.cmd_.data = [-0.5,0.5]

                    else :
                        self.cmd_.data = [Speed,Speed] # retourne  --  Arret 0.0 0.0

            else :
                self.cmd_.data = [0.0,0.0]
                
##########################################################################
#Lasers & Wall Detection
##########################################################################


            if self.Laser2val <2.5:
                self.cmd_.data = [-1.0, -1.0] # Avance
               
            if self.Laser3val <2.5:
                self.cmd_.data = [-1.0, -1.0] # Avance
          
            if self.Laser4val <2.5:
                self.cmd_.data = [-1.0, -1.0] # Avance

            if self.Laser5val <2.5:
                self.cmd_.data = [-1.0, -1.0] # Avance




          
        # now call function we defined above
#        clear()
        self.cmd_publisher_.publish(self.cmd_)
        



def main(args=None):
    rclpy.init(args=args)

    epuck_controller = EPuckController()

    try:
        rclpy.spin(epuck_controller)
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()





















