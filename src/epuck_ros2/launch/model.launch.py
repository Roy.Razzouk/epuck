import launch
from launch.substitutions import Command, LaunchConfiguration
import launch_ros
import os


def generate_launch_description():
    pkg_share = launch_ros.substitutions.FindPackageShare(
        package='epuck_ros2').find('epuck_ros2')
    model_path = os.path.join(pkg_share, 'urdf/epuck.urdf.xacro')

    robot_state_publisher_node = launch_ros.actions.Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        parameters=[{'robot_description': Command(['xacro ', model_path])}]
    )

    return launch.LaunchDescription([
        robot_state_publisher_node
    ])
